# Monitoramento de Pipeline de Entrega com GIT

## Autor
- Rondinelle Walitte

## Objetivos da Aula Prática
O objetivo desta aula prática é simular o monitoramento do processo de pipeline de entrega utilizando o GIT para compreender os aspectos fundamentais do pipeline na construção automática de uma aplicação.

## Procedimentos Práticos
Durante a aula, realizamos as seguintes atividades para monitorar o processo de pipeline de entrega:

1. **Instalação do Sistema GIT:** Garantir que o GIT esteja instalado e configurado corretamente em nosso ambiente de desenvolvimento.
2. **Simulação de Execução do Job:** Realizar simulações para verificar se houve sucesso ou falha na execução do job dentro do pipeline.

### Atividade Proposta
- **Entender o Script de Integração Contínua:** Analisar e compreender como funciona o script responsável pela chamada de Integração Contínua no processo.
- **Elaboração de Relatório:** Criar um relatório detalhado ao final da atividade, cobrindo todos os aspectos essenciais do pipeline.

## Checklist
- [x] Instalar o sistema GIT.
- [ ] Simular sucesso na execução do job.
- [ ] Simular falha na execução do job.

## Resultados
O relatório final da aula prática deve conter as seguintes seções:

### Introdução
Descreva brevemente o propósito do monitoramento do pipeline de entrega e a importância do uso do GIT neste contexto.

### Métodos
Explique os métodos utilizados para simular o processo de pipeline, incluindo detalhes técnicos sobre a configuração do GIT e a execução das simulações.

### Resultados
Discuta os resultados obtidos nas simulações, destacando casos de sucesso e falha na execução do pipeline.

### Conclusão
Ofereça uma visão geral sobre o aprendizado adquirido durante a aula prática e como o conhecimento do pipeline de entrega pode ser aplicado no desenvolvimento de software.

---

Para mais informações sobre como utilizar este projeto, consulte a documentação oficial do GIT ou entre em contato com o autor..
